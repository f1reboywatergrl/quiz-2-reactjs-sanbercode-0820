import React from 'react';
import logo from './logo.svg';
import './App.css';
import Data from './data.js';

function App() {
  return (
    <div className="App">
      <div>
        <Data />
      </div>
    </div>
  );
}

export default App;
