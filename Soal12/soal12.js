class BangunDatar {
	constructor(nama){
		this._nama = nama;
	}
	get nama(){
		return this._nama;
	}
	set nama(x){
		this._nama=x;
	}
	luas(s){
		return ("");
	} 
	keliling(s){
		return ("");
	}
}

class lingkaran extends BangunDatar{
	constructor(nama){
		super(nama);
	}
	luas(r){
		return(3.14*r*r);
	}
	keliling(r){
		return(2*3.14*r);
	}
}	

class persegi extends BangunDatar{
	constructor(nama){
		super(nama);
	}
	luas(s){
		return(s*s);
	}
	keliling(r){
		return(4*r);
	}
}

bd = new BangunDatar("A");
lingk = new lingkaran("L");
pers = new persegi("P");

console.log(bd.nama);
console.log(bd.luas(1));
console.log(bd.keliling(2));
bd.nama = "B";
console.log(bd.nama);

console.log(lingk.nama);
console.log(lingk.luas(1));
console.log(lingk.keliling(2));
lingk.nama="Lk";
console.log(lingk.nama);

console.log(pers.nama);
console.log(pers.luas(1));
console.log(pers.keliling(2));
pers.nama="Ps";
console.log(pers.nama);